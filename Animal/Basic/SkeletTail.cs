﻿
namespace Animal.Basic
{
    class SkeletTail
    {
        public int LengthTail { get; set; } ///длина хвоста

        public SkeletTail()
        {
            LengthTail = 50;
        }
        public SkeletTail(int lengthTail)
        {
            LengthTail = lengthTail;
        }
        public override string ToString()
        {
            return $"Длина хвоста в метрах: {LengthTail} ";
        }
    }
}
