﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animal.Basic
{
    class SkeletBody
    {
        public int LengthBody { get; set; } ///длина тела
        public int WidthBody { get; set; } ///ширина грудины
        
        public SkeletBody()
        {
            LengthBody = 500;
            WidthBody = 40;
        }
        public SkeletBody(int lengthBody, int widthBody)
        {
            LengthBody = lengthBody;
            WidthBody = widthBody;
        }
        public override string ToString()
        {
            return $"Длина тела: {LengthBody} \nШирина грудины: {WidthBody}";
        }
    }
}
