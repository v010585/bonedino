﻿
namespace Animal.Basic
{
    class SkeletHead
    {

        public virtual int TeethCount { get; set; } /// количество зубов

        public SkeletHead()
        {
            TeethCount = 200;
        }    
        public SkeletHead(int teethCount)
        {
            TeethCount = teethCount;
        }
        public override string ToString()
        {
            return $"Количество зубов: {TeethCount}"; 
        }
    }
}
