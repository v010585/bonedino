﻿
namespace Animal.Basic
{
    class Skelet
    {
        public SkeletHead skelethead;
        public SkeletBody skeletbody;
        public SkeletForwardFinitenesses skeletForward;
        public SkeletHindFinitenesses skeletHind;
        public SkeletTail skeletTail;

        public Skelet(SkeletHead skelethead,
            SkeletBody skeletbody,
            SkeletHindFinitenesses skeletHind,
            SkeletForwardFinitenesses skeletForward,
            SkeletTail skeletTail)
        {
            

            this.skelethead = skelethead;
            this.skeletbody = skeletbody;
            this.skeletForward = skeletForward;
            this.skeletHind = skeletHind;
            this.skeletTail = skeletTail;

        }

        
    }
}
