﻿
namespace Animal.Basic
{
    class SkeletHindFinitenesses
    {
        public int Paw { get; set; } /// лапа передняя

        public SkeletHindFinitenesses()
        {
            Paw = 2;
        }
        public SkeletHindFinitenesses(int paw)
        {
            Paw = paw;
        }
        public override string ToString()
        {
            return $"Колличество передних лап: {Paw}";
        }
    }
}
