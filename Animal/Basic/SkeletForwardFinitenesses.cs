﻿
namespace Animal.Basic
{
    class SkeletForwardFinitenesses
    {
        public int Paw { get; set; } /// лапа задняя

        public SkeletForwardFinitenesses()
        {
            Paw = 2;
        }
        public SkeletForwardFinitenesses(int paw)
        {
            Paw = paw;
        }
        public override string ToString()
        {
            return $"Колличество задних лап: {Paw}";
        }

    }
}
