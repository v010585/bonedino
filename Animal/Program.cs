﻿using Animal.Basic;
using System;

namespace Animal
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Приветствую юный некромант!! Ты принес части тел для создания нежити!");
            bool chose = IsCreateCustomDino();
           
            Skelet dragonSkelet = new Skelet(
               PrepareHead(chose),
               PrepareBody(chose),
               PrepareHintFinit(chose),
               PrepareForwardFinit(chose),
               PrepareTail(chose));
            Console.WriteLine("Поздравляю юный некромант ты создал нежить...МУ ХА ХА ХА!!!");
           
            Console.WriteLine(dragonSkelet.skelethead);
            Console.WriteLine(dragonSkelet.skeletbody);
            Console.WriteLine(dragonSkelet.skeletHind);
            Console.WriteLine(dragonSkelet.skeletForward);
            Console.WriteLine(dragonSkelet.skeletTail);

            Console.WriteLine("При дуновении ветра ваша нежить развалилась(((");
            Console.WriteLine("Вы сидите в углу в позе эмбриона рисуя круги на полу(((");

            Console.ReadKey();
        }

        private static bool IsCreateCustomDino()
        {
            bool chose = false;
            Console.WriteLine("1 - Размер частей тела по умолчанию ");
            Console.WriteLine("2 - Указать своё количество ");
            string input = Console.ReadLine();

            if (int.TryParse(input, out int result))
            {
                 chose = result == 2;

            }
            return chose;
        }

        private static SkeletHead PrepareHead(bool isCustomChose)
        {
            SkeletHead head = new SkeletHead();
            if (isCustomChose)
            {
                Console.WriteLine("Введите количество зубов: ");
                int teethCount = int.Parse(Console.ReadLine());
                head = new SkeletHead(teethCount);
            }
            return head;
        }
        
        private static SkeletBody PrepareBody(bool isCustomChose)
        {
            SkeletBody skeletbody = new SkeletBody();
            if (isCustomChose)
            {
                Console.WriteLine("Введите длину тела в метрах: ");
                int lengthBody = int.Parse(Console.ReadLine());
                Console.WriteLine("Введите ширину тела в метрах: ");
                int widthBody = int.Parse(Console.ReadLine());
                skeletbody = new SkeletBody(lengthBody, widthBody);
            }
            return skeletbody;
        }
        
        private static SkeletHindFinitenesses PrepareHintFinit(bool isCustomChose)
        {
            SkeletHindFinitenesses skeletHind = new SkeletHindFinitenesses();
            if (isCustomChose)
            {
                Console.WriteLine("Введите количество передних лап: ");
                int paw = int.Parse(Console.ReadLine());
                skeletHind = new SkeletHindFinitenesses(paw);
            }
            return skeletHind;
        }

        private static SkeletForwardFinitenesses PrepareForwardFinit(bool isCustomChose)
        {
            SkeletForwardFinitenesses skeletForward = new SkeletForwardFinitenesses();
            if (isCustomChose)
            {
                Console.WriteLine("Введите количество задних лап: ");
                int paw = int.Parse(Console.ReadLine());
                skeletForward = new SkeletForwardFinitenesses(paw);
            }
            return skeletForward;
        }

        private static SkeletTail PrepareTail(bool isCustomChose)
        {
            SkeletTail skeletTail = new SkeletTail();
            if (isCustomChose)
            {
                Console.WriteLine("Введите длину хвоста в метрах: ");
                int lengthTail = int.Parse(Console.ReadLine());
                skeletTail = new SkeletTail(lengthTail);
            }
            return skeletTail;
        }
    }

}
